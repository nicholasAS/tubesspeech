from flask import Flask, render_template, request, send_file
from pydub import AudioSegment
import os
import scipy.io.wavfile as wav
import numpy
import matplotlib
import sys
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.fftpack import dct
from tensorflow import keras
import cv2

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")

@app.route('/predict', methods=['GET', 'POST'])
def predict():
    f = request.files['audio_data']
    with open('audio.wav', 'wb') as audio:
        f.save(audio)
        print('file uploaded successfully')
    PATH = 'Audio/chunks/'
    PATH_SPECTO = 'Spectograms/'
    # Remove File Sebelumnya
    for f in os.listdir(PATH):
        os.remove(os.path.join(PATH, f))
    for f in os.listdir(PATH_SPECTO):
        os.remove(os.path.join(PATH_SPECTO, f))
    audio = "audio.wav"
    name='sample'
    audio_file = AudioSegment.from_wav(audio)
    multi_split(audio_file,1,name)
    save_plot(PATH)
    
    # Get Spectogram Sample
    test_images = []
    samples = os.listdir(PATH_SPECTO)
    print(samples)
    for sample in samples:
        print(os.path.join(PATH_SPECTO,sample))
        im = cv2.imread(os.path.join(PATH_SPECTO,sample))
        im = cv2.resize(im, (232, 44))
        im = im/255
        test_images.append(im)
    test_data = numpy.array(test_images)
    
    # Load dan Predict
    used_model =keras.models.load_model('my_model2')
    arr=numpy.round(used_model.predict(test_data))  
    
    # Klasifikasi Hasil
    arr=arr.tolist()
    count_one = 0
    counter = [0,0,0,0,0,0]
    for item in arr:
        print(item)
        for idx, val in enumerate(item):
            if val==1.0:
                counter[idx]+=1
                count_one+=1
    labels = ['Hanjaya', 'Jennifer', 'Michelle', 'Nicholas', 'Eirenika','Daniel']
    response = "<h1 class='cyan'>"+labels[numpy.argmax(counter)]+"</h1><p>"
    response += str(counter[0]/count_one*100) + "% Hanjaya <br>"
    response += str(counter[1]/count_one*100) + "% Jennifer <br>"
    response += str(counter[2]/count_one*100) + "% Michelle <br>"
    response += str(counter[3]/count_one*100) + "% Nicholas <br>"
    response += str(counter[4]/count_one*100) + "% Eirenika <br>"
    response += str(counter[5]/count_one*100) + "% Daniel <br></p>"
    print(response)
    colors = ['gold', 'yellowgreen', 'lightcoral', 'lightskyblue','purple','orangered']
    
    plt.close('all')
    # Plot Diagram Pie
    plt.pie(counter, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)
    plt.legend(labels=labels)
    plt.axis('equal')
    plt.savefig('pie.png',transparent=True)
    sys.stdout.flush()
    
    return response

#Get Pie Diagram Image
@app.route('/pie', methods=['GET', 'POST'])
def pie():
    return send_file('pie.png',  mimetype='image/png')

# Split
def single_split(audio,from_time,to_time,filename):
    t1 = from_time*1000
    t2 = to_time*1000
    split_audio = audio[t1:t2]
    split_audio.export(filename,format="wav")

def multi_split(audio,per_split,name):
    limit_seconds = len(audio)
    temp = 0
    counter = 0
    while True:
        filename = 'Audio/chunks/' + name + '_' + str(counter) + '.wav'
        single_split(audio,temp,temp+per_split,filename)
        print(str(counter)+ '.wav has been succesfully saved !')
        counter += 1
        temp = temp + per_split
        if ((temp*1000) > len(audio)):
            break

# Get Spectogram
def save_plot(directory):
    audio_files = os.listdir(directory)
    counter = 0
    for audio_file in audio_files:
        print(counter)
        counter+=1
        filename = os.path.join(directory,audio_file)
        
        
        # load audio
        sample_rate, signal = wav.read(filename)  
        
       
         # Pre emphasis
        pre_emphasis = 0.97
        try:
            emphasized_signal = numpy.append(signal[0], signal[1:] - pre_emphasis * signal[:-1])
        except:
            continue
        # Framing: membagi signal ke dalam window-window yang saling overlap
        frame_size = 0.025
        frame_stride = 0.01
        frame_length, frame_step = frame_size * sample_rate, frame_stride * sample_rate  # Convert from seconds to samples
        signal_length = len(emphasized_signal)
        frame_length = int(round(frame_length))
        frame_step = int(round(frame_step))
        num_frames = int(numpy.ceil(float(numpy.abs(signal_length - frame_length)) / frame_step))  # Make sure that we have at least 1 frame

        pad_signal_length = num_frames * frame_step + frame_length
        z = numpy.zeros((pad_signal_length - signal_length))
        pad_signal = numpy.append(emphasized_signal,z)  # Pad Signal to make sure that all frames have equal number of samples without truncating any samples from the original signal
        indices = numpy.tile(numpy.arange(0, frame_length), (num_frames, 1)) + numpy.tile(numpy.arange(0, num_frames * frame_step, frame_step), (frame_length, 1)).T
        frames = pad_signal[indices.astype(numpy.int32, copy=False)]
        
        # Window: menjalankan fungsi Hamming window pada setiap frame, dibantu dengan library numpy
        frames *= numpy.hamming(frame_length)
        
        # Fourier Transform dan Power Spectrum: melakukan fourier transform sebanyak N point pada setiap frame
        NFFT = 512
        mag_frames = numpy.absolute(numpy.fft.rfft(frames, NFFT))  
        pow_frames = ((1.0 / NFFT) * (mag_frames ** 2))  
        
        energy = numpy.sum(pow_frames, 1)
        
        # Filter Banks: mengkonversi (filter) frekuensi ke dalam persepsi manusia (variasi tinggi pada frekuensi rendah dan variasi rendah pada frekuensi tinggi)
        nfilt = 26
        low_freq_mel = 0
        high_freq_mel = (2595 * numpy.log10(1 + (sample_rate / 2) / 700))  # Convert Hz to Mel
        mel_points = numpy.linspace(low_freq_mel, high_freq_mel, nfilt + 2)  # Equally spaced in Mel scale
        hz_points = (700 * (10 ** (mel_points / 2595) - 1))  # Convert Mel to Hz
        bin = numpy.floor((NFFT + 1) * hz_points / sample_rate)

        fbank = numpy.zeros((nfilt, int(numpy.floor(NFFT / 2 + 1))))
        for m in range(1, nfilt + 1):
            f_m_minus = int(bin[m - 1])  # left
            f_m = int(bin[m])  # center
            f_m_plus = int(bin[m + 1])  # right

            for k in range(f_m_minus, f_m):
                fbank[m - 1, k] = (k - bin[m - 1]) / (bin[m] - bin[m - 1])
            for k in range(f_m, f_m_plus):
                fbank[m - 1, k] = (bin[m + 1] - k) / (bin[m + 1] - bin[m])
        filter_banks = numpy.dot(pow_frames, fbank.T)
        filter_banks = numpy.where(filter_banks == 0, numpy.finfo(float).eps, filter_banks)  # Numerical Stability
        filter_banks = numpy.log(filter_banks)  # dB
        
        # IDFT: Inverse DFT, mengembalikan signal hasil DFT menjadi fitur
        plt.subplot(312)
        num_ceps = 12
        cep_lifter = 22
        mfcc = dct(filter_banks, type=2, axis=1, norm='ortho')[:, : (num_ceps + 1)]  # Keep 2-13
        (nframes, ncoeff) = mfcc.shape
        n = numpy.arange(ncoeff)
        lift = 1 + (cep_lifter / 2) * numpy.sin(numpy.pi * n / cep_lifter)
        mfcc *= lift  
        mfcc[:, 0] = numpy.log(energy)
        
        # Plot Spectrogram
        plt.imshow(filter_banks.T, cmap=plt.cm.jet, aspect='auto')
        ax = plt.gca()
        ax.invert_yaxis()
        plt.axis('off')
        plt.savefig('Spectograms/'+ audio_file+ '.png', dpi=50, transparent=True,bbox_inches='tight',pad_inches=0)
        
if __name__ == "__main__":
    app.run(debug=True)